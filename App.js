import React from 'react';
import {SafeAreaView, StyleSheet} from 'react-native';
import {Provider} from 'react-redux';
import {PersistGate} from 'redux-persist/integration/react';
import Todos from './src/container';
import {store, Persistor} from './src/store';

function App() {
  return (
    <Provider store={store}>
      <PersistGate loading={null} persistor={Persistor}>
        <SafeAreaView style={styles.root}>
          <Todos />
        </SafeAreaView>
      </PersistGate>
    </Provider>
  );
}

export default App;

const styles = StyleSheet.create({
  root: {
    backgroundColor: '#263238',
    flex: 1,
  },
});
