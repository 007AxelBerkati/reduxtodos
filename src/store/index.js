import AsyncStorage from '@react-native-async-storage/async-storage';
import {applyMiddleware, createStore} from 'redux';
import persistReducer from 'redux-persist/es/persistReducer';
import persistStore from 'redux-persist/es/persistStore';
import ReduxThunk from 'redux-thunk';
import Reducers from '../reducers';
import reduxLogger from 'redux-logger';

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
};

const configPersist = persistReducer(persistConfig, Reducers);

export const store = createStore(
  configPersist,
  applyMiddleware(ReduxThunk, reduxLogger),
);
export const Persistor = persistStore(store);
