export const FETCH_TODO_SUCCESS = '@FETCH_TODO_SUCCESS';
export const TAMBAH_TODO = '@TAMBAH_TODO';
export const HAPUS_TODO = '@HAPUS_TODO';
export const UPDATE_TODO = '@UPDATE_TODO';
export const DELETE_TODO = '@DELETE_TODO';

// LOADING
export const SET_LOADING = '@SET_LOADING';
