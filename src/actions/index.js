import axios from 'axios';
import {Alert} from 'react-native';
import {useDispatch} from 'react-redux';

import {
  DELETE_TODO,
  FETCH_TODO_SUCCESS,
  SET_LOADING,
  TAMBAH_TODO,
  UPDATE_TODO,
} from '../types';

export const saveTodos = data => ({
  type: FETCH_TODO_SUCCESS,
  payload: data,
});

export const addTodos = data => ({
  type: TAMBAH_TODO,
  payload: data,
});

export const updateTodos = data => ({
  type: UPDATE_TODO,
  payload: data,
});

export const deleteTodos = id => ({
  type: DELETE_TODO,
  payload: id,
});

export const setLoading = value => ({
  type: SET_LOADING,
  payload: value,
});

export const getTodos = () => async dispatch => {
  dispatch(setLoading(true));
  const resTodos = await axios.get('http://code.aldipee.com/api/v1/todos');
  if (resTodos.data.results.length > 0) {
    dispatch(saveTodos(resTodos.data.results));
  }
};

export const DeleteTodos = id => async dispatch => {
  try {
    await axios.delete(`http://code.aldipee.com/api/v1/todos/${id}`);
    dispatch(deleteTodos(id));
  } catch (err) {
    console.log(err);
  }
};

export const TambahTodos = data => async dispatch => {
  try {
    await axios.post(`http://code.aldipee.com/api/v1/todos`, data).then(() => {
      Alert.alert('sukses', 'Data berhasil ditambahkan');
    });
    dispatch(addTodos(data));
  } catch (err) {
    console.log(err);
  }
};
export const EditTodos = (id, data) => async dispatch => {
  try {
    await axios
      .patch(`http://code.aldipee.com/api/v1/todos/${id}`, data)
      .then(() => {
        Alert.alert('sukses', 'Data berhasil diupdate');
      });
    dispatch(updateTodos(data));
  } catch (err) {
    console.log(err);
  }
};
