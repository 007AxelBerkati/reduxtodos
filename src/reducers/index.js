import {
  DELETE_TODO,
  FETCH_TODO_SUCCESS,
  SET_LOADING,
  TAMBAH_TODO,
  UPDATE_TODO,
} from '../types';

const initialState = {
  todos: [],
  loading: true,
};

const Reducers = (state = initialState, action) => {
  switch (action.type) {
    case FETCH_TODO_SUCCESS:
      return {
        ...state,
        todos: action.payload,
        loading: false,
      };
    case DELETE_TODO:
      return {
        ...state,
        todos: state.todos.filter(el => el.id !== action.payload),
      };

    case TAMBAH_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };
    case UPDATE_TODO:
      return {
        ...state,
        todos: [...state.todos, action.payload],
      };
    case SET_LOADING:
      return {
        ...state,
        loading: action.payload,
      };
    default:
      return state;
  }
};

export default Reducers;
